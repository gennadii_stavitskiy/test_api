﻿Code src:
https://bitbucket.org/gennadii_stavitskiy/test_api/src/master/ 

--------------------
Test-API
--------------------
The Api solution represents simple web base api for:
1) Create/Get/List users;
2) Create/Get/List accounts;
3) Validate several busines rules;

Solution file (VS2019): \src\Api.sln
Source code: \src

Solution projects:
 - Api - .NET Core 2.0 web api with mvc approach
 - Backend - .Net Core 2.0 class library includes backend services such as UserDataService, AccountDataService, Models, dB contexts, Validation rules (RuleFactory, rule strategies).
 
 Test projects (xUnit):
 - Backend.Tests

Dependencies:
DynamoDb-Local.
docker-compose.yml

--------
WEB API
--------

URL: <host:port>/api/ (http://localhost:12096/api/)

---------
METHODS:
---------
GET /api/users/init - Create schema for users
GET /api/users - Fetch all users
GET /api/users/{id} - Fetch user with id
POST /api/users - create new user

GET /api/accounts/init - Create schema for accounts
GET /api/accounts - Fetch all accounts
GET /api/accounts/{id} - Fetch accounts with id
POST /api/users - create new user

*GET
Code: 200 ok
Code: 404 (NotFound) if result is null or empty
Code: 500 (InternalServerError) If exception was thrown during fetching movies

* POST (CREATE)
---------------

POST /api/movies - create new movie with provided attributes. A movieId will be generated and new created movie return
Parameters: 
{
  "Id": "string",
  "Name": "string",
  "Email": "string,
  "MonthlySalary": 0,
  "MonthlyExpenses": 0
}

Response:
Code:201 Created
Content: 
{
  "Id": "string",
  "Name": "string",
  "Email": "string,
  "MonthlySalary": 0,
  "MonthlyExpenses": 0
}

Code: 404 (NotFound) - if result is null or empty
Code: 400 (BadReuest) - if model is not valid
Code: 500 (InternalServerError) - If exception was thrown during creating movie

------
 NOTE
------
The Api project includes Swagger tools for documenting APIs built on ASP.NET Core API
It could be useful for starting familiar with API and also for simple tests.
To fetch doc: http://localhost:12096/swagger/

----------------------------
 Deployment & init
----------------------------
Docker-Compose.yml
After deploying to create dB schema the next api calls should be done:
api/users/init - created tables for users
api/accounts/init - create table for account

-----------------
Design Decisions 
-----------------
Most of design decisions in this project are related to Microsoft Best Practice approaches with following SOLID principles.
The ASP.NET Core dependency injection container was used in this case.

------------------------------------------------------------------------------------------------------