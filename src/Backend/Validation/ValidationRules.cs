﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

namespace Backend.Validation
{
    public enum ValidationRules
    {
        [Description("Email not unique")] UniqueEmail,
        [Description("Monthly salary must be positive")] MonthlySalaryPositive,
        [Description("Monthly expenses must be positive")] MonthlyExpensesPositive,
        [Description("Monthly salary and monthly expenses balance must be greater than $1000")] MonthlySalaryAndMonthlyExpensesBalanceLessThan1000
    }

    public static class ValidationRulesExtension
    {
        public static string Description(this Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            if (fi.GetCustomAttributes(typeof(DescriptionAttribute), false) is DescriptionAttribute[] attributes 
                && attributes.Any())
            {
                return attributes.First().Description;
            }

            return value.ToString();
        }
    }
}
