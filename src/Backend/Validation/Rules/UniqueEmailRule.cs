﻿using Backend.Models;
using Backend.Repositories.DynamoDb;
using System.Threading.Tasks;

namespace Backend.Validation.Rules
{
    public class UniqueEmailRule: IRuleStrategy
    {
        private readonly IDynamoDbContext<EmailToUser> emailToUserContext;

        public UniqueEmailRule(IDynamoDbContext<EmailToUser> emailToUserContext)
        {
            this.emailToUserContext = emailToUserContext;
        }

        public async Task<bool> Check(User user)
        {
            var item = await emailToUserContext.GetByIdAsync(user.Email);

            return item == null;
        }
    }
}
