﻿using System.Threading.Tasks;
using Backend.Models;

namespace Backend.Validation.Rules
{
    public class MonthlyBalanceRule : IRuleStrategy
    {
        public async Task<bool> Check(User user)
        {
            return (user.MonthlySalary - user.MonthlyExpenses) >= 1000;
        }
    }
}
