﻿using System.Threading.Tasks;
using Backend.Models;

namespace Backend.Validation.Rules
{
    public class MonthlySalaryPositiveRule: IRuleStrategy
    {
        public async Task<bool> Check(User user)
        {
            return user.MonthlySalary > 0;
        }
    }
}
