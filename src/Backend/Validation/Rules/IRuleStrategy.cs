﻿using System.Threading.Tasks;
using Backend.Models;

namespace Backend.Validation.Rules
{
    public interface IRuleStrategy
    {
        Task<bool> Check(User user);
    }
}
