﻿using System.Threading.Tasks;
using Backend.Models;

namespace Backend.Validation.Rules
{
    public class MonthlyExpensesPositiveRule : IRuleStrategy
    {
        public async Task<bool> Check(User user)
        {
            return user.MonthlyExpenses > 0;
        }
    }
}
