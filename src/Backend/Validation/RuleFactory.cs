﻿using Backend.Models;
using Backend.Repositories.DynamoDb;
using Backend.Validation.Rules;
using System;

namespace Backend.Validation
{
    public class RuleFactory
    {
        private IDynamoDbContext<EmailToUser> emailToUserContext;

        public RuleFactory(IDynamoDbContext<EmailToUser> emailToUserContext)
        {
            this.emailToUserContext = emailToUserContext;
        }
        public IRuleStrategy Get(ValidationRules rule)
        {
            switch (rule)
            {
                case ValidationRules.UniqueEmail:
                    return new UniqueEmailRule(emailToUserContext);
                case ValidationRules.MonthlySalaryPositive:
                    return new MonthlySalaryPositiveRule();
                case ValidationRules.MonthlyExpensesPositive:
                    return new MonthlyExpensesPositiveRule();
                case ValidationRules.MonthlySalaryAndMonthlyExpensesBalanceLessThan1000:
                    return new MonthlyBalanceRule();
                default:
                    throw new ArgumentOutOfRangeException(nameof(rule), rule, null);
            }
        }
    }
}
