﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Backend.Models;
using Backend.Repositories.DynamoDb;
using Backend.Validation;

namespace Backend.Services
{
    public class UsersDataService
    {
        private readonly RuleFactory ruleFactory;
        private readonly IDynamoDbContext<User> userContext;
        private readonly IDynamoDbContext<EmailToUser> emailToUserContext;

        public UsersDataService(IDynamoDbContext<User> userContext, IDynamoDbContext<EmailToUser> emailToUserContext)
        {
            this.userContext = userContext;
            this.emailToUserContext = emailToUserContext;
            ruleFactory = new RuleFactory(emailToUserContext);
        }

        public async Task<User> GetUser(string id)
        {
            return await userContext.GetByIdAsync(id);
        }

        public async Task<IEnumerable<User>> ListUsers()
        {
            return await userContext.GetAllAsync();
        }

        public async Task<ResultModel<User>> CreateUser(User user)
        {
            var validationRules = new[]
            {
                ValidationRules.UniqueEmail,
                ValidationRules.MonthlyExpensesPositive,
                ValidationRules.MonthlySalaryPositive
            };

            var result = await Validate(user, validationRules);

            if (!result.IsValid)
            {
                return result;
            }

            user.Id = Guid.NewGuid().ToString();

            await userContext.SaveAsync(user);

            var emailToUser = new EmailToUser
            {
                Id = user.Email,
                UserId = user.Id
            };

            await emailToUserContext.SaveAsync(emailToUser);

            result.Result = user;

            return result;
        }

        public async Task<ResultModel<User>> Validate(User user, IEnumerable<ValidationRules> validationRules)
        {
            var result = new ResultModel<User>();

            var tasks = new List<Task>();

            foreach (var rule in validationRules)
            {
                tasks.Add(Task.Run(async () => await CheckRule(rule, user, result)));
            }

            await Task.WhenAll(tasks.ToArray());

            return result;
        }

        public async Task Init()
        {
            await userContext.InitializeSchema();
            await emailToUserContext.InitializeSchema();
        }

        private async Task CheckRule(ValidationRules rule, User user, ResultModel<User> result)
        {
            if (!await ruleFactory.Get(rule).Check(user))
            {
                result.FailedRules.Add(rule);
            }
        }
    }
}