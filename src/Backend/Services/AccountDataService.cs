﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Backend.Models;
using Backend.Repositories.DynamoDb;
using Backend.Validation;

namespace Backend.Services
{
    public class AccountDataService
    {
        private readonly IDynamoDbContext<Account> accountContext;
        private readonly UsersDataService usersDataService;

        public AccountDataService(IDynamoDbContext<Account> accountContext, UsersDataService usersDataService)
        {
            this.accountContext = accountContext;
            this.usersDataService = usersDataService;
        }

        public async Task<Account> GetAccount(string id)
        {
            return await accountContext.GetByIdAsync(id);
        }

        public async Task<IEnumerable<Account>> ListAccount()
        {
            return await accountContext.GetAllAsync();
        }

        public async Task<ResultModel<Account>> CreateAccount(Account account)
        {
            var validationRules = new[]
            {
                ValidationRules.MonthlySalaryAndMonthlyExpensesBalanceLessThan1000
            };

            var user = await usersDataService.GetUser(account.UserId);

            var userValidationResult = await usersDataService.Validate(user, validationRules);

            var result = new ResultModel<Account>();

            if (!userValidationResult.IsValid)
            {
                result.FailedRules = userValidationResult.FailedRules;
                return result;
            }

            account.Id = Guid.NewGuid().ToString();

            await accountContext.SaveAsync(account);
            
            result.Result = account;

            return result;
        }
        
        public async Task Init()
        {
            await accountContext.InitializeSchema();
        }
    }
}