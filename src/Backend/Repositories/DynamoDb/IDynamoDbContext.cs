﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Backend.Repositories.DynamoDb
{
    public interface IDynamoDbContext<T> : IDisposable where T : class
    {
        Task<T> GetByIdAsync(string id);
        Task SaveAsync(T item);
        Task<IList<T>> GetAllAsync();
        Task InitializeSchema();
    }
}
