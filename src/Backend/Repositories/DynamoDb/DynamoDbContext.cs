﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.Model;

namespace Backend.Repositories.DynamoDb
{
    public class DynamoDbContext<T> : DynamoDBContext, IDynamoDbContext<T>
        where T : class
    {
        private readonly IAmazonDynamoDB client;
        private readonly DynamoDBOperationConfig config;
        private readonly string tableName;

        public DynamoDbContext(IAmazonDynamoDB client, string tableName)
            : base(client)
        {
            this.client = client;
            this.tableName = tableName;

            config = new DynamoDBOperationConfig
            {
                OverrideTableName = tableName
            };
        }

        public async Task<T> GetByIdAsync(string id)
        {
            return await LoadAsync<T>(id, config);
        }

        public async Task<IList<T>> GetAllAsync()
        {
            var conditions = new List<ScanCondition>();
            return await ScanAsync<T>(conditions).GetRemainingAsync();
        }

        public async Task SaveAsync(T item)
        {
            await base.SaveAsync(item, config);
        }

        public async Task DeleteByIdAsync(T item)
        {
            await DeleteAsync(item, config);
        }

        public async Task InitializeSchema()
        {
            var request = new ListTablesRequest
            {
                Limit = 10
            };

            var response = await client.ListTablesAsync(request);

            var results = response.TableNames;

            if (!results.Contains(tableName))
            {
                var createRequest = new CreateTableRequest
                {
                    TableName = tableName,
                    AttributeDefinitions = new List<AttributeDefinition>
                    {
                        new AttributeDefinition
                        {
                            AttributeName = "Id",
                            AttributeType = "S"
                        }
                    },
                    KeySchema = new List<KeySchemaElement>
                    {
                        new KeySchemaElement
                        {
                            AttributeName = "Id",
                            KeyType = "HASH"
                        }
                    },
                    ProvisionedThroughput = new ProvisionedThroughput
                    {
                        ReadCapacityUnits = 2,
                        WriteCapacityUnits = 2
                    }
                };

                await client.CreateTableAsync(createRequest);
            }
        }
    }
}