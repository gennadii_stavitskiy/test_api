﻿using System.Collections.Generic;
using System.Linq;
using Backend.Validation;

namespace Backend.Models
{
    public class ResultModel<T>
    {
        public T Result { get; set; }

        public List<ValidationRules> FailedRules { get; set; } = new List<ValidationRules>();

        public bool IsValid => FailedRules.Count == 0;

        public string GetError()
        {
            return string.Join(';', FailedRules.Select(r => r.Description()));
        }
    }
}
