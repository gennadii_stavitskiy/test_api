﻿using Amazon.DynamoDBv2.DataModel;

namespace Backend.Models
{
    [DynamoDBTable("Accounts")]
    public class Account
    {
        [DynamoDBHashKey]
        public string Id { get; set; }
        [DynamoDBProperty]
        public string UserId { get; set; }        
    }
}
