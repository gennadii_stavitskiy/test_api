﻿using System.ComponentModel.DataAnnotations;
using Amazon.DynamoDBv2.DataModel;

namespace Backend.Models
{
    [DynamoDBTable("Users")]
    public class User
    {
        [DynamoDBHashKey]
        public string Id { get; set; }
        [DynamoDBProperty]
        [Required]
        public string Name { get; set; }
        [DynamoDBProperty]
        [Required]
        public string Email { get; set; }
        [DynamoDBProperty]
        public long MonthlySalary { get; set; }
        [DynamoDBProperty]
        public long MonthlyExpenses { get; set; }
    }
}
