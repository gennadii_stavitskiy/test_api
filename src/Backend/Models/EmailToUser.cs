﻿using Amazon.DynamoDBv2.DataModel;

namespace Backend.Models
{
    [DynamoDBTable("EmailToUser")]
    public class EmailToUser
    {
        [DynamoDBHashKey]
        public string Id { get; set; }
        [DynamoDBProperty]
        public string UserId { get; set; }        
    }
}
