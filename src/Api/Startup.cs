﻿using Amazon.DynamoDBv2;
using Backend.Models;
using Backend.Repositories.DynamoDb;
using Backend.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Swashbuckle.AspNetCore.Swagger;

namespace Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("AllowAllHeaders",
                      builder =>
                      {
                          builder.AllowAnyOrigin()
                                 .AllowAnyHeader()
                                 .AllowAnyMethod();
                      });
            });

            services.AddMvc();
            
            services.AddTransient<UsersDataService, UsersDataService>();
            services.AddTransient<AccountDataService, AccountDataService>();
            

            var dynamoDbConfig = Configuration.GetSection("DynamoDb");
            var runLocalDynamoDb = dynamoDbConfig.GetValue<bool>("LocalMode");
            
            var awsOptions = Configuration.GetAWSOptions();
            if (runLocalDynamoDb)
            {
                awsOptions.DefaultClientConfig.ServiceURL = dynamoDbConfig.GetValue<string>("LocalServiceUrl");
            }

            services.AddDefaultAWSOptions(awsOptions);

            var client = awsOptions.CreateServiceClient<IAmazonDynamoDB>();
            
            services.AddScoped<IDynamoDbContext<User>>(provider => new DynamoDbContext<User>(client, "Users"));
            services.AddScoped<IDynamoDbContext<Account>>(provider => new DynamoDbContext<Account>(client, "Accounts"));
            services.AddScoped<IDynamoDbContext<EmailToUser>>(provider => new DynamoDbContext<EmailToUser>(client, "EmailToUser"));
            
            // Register the Swagger generator, defining one or more Swagger documents
            services.AddSwaggerGen(c => { c.SwaggerDoc("v1", new Info { Title = "My API", Version = "v1" }); });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory) //, IOptions<CorsOptionsSettings> corsSettings)
        {
            //var origins = corsSettings.Value.CorsOptionOrigin;

            //app.UseCors(options => options.WithOrigins(origins).AllowAnyMethod());
            app.UseCors("AllowAllHeaders");

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c => { c.SwaggerEndpoint("/swagger/v1/swagger.json", "API V1"); });

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();
        }
    }
}
