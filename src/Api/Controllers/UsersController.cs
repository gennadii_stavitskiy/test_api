﻿using Backend.Models;
using Backend.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly UsersDataService usersDataService;
        private readonly ILogger logger;

        public UsersController(UsersDataService usersDataService, ILoggerFactory factory)
        {
            this.usersDataService = usersDataService;
            this.logger = factory.CreateLogger("Api.UserController");
        }

        // GET api/users
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                var users = await usersDataService.ListUsers();

                return Ok(users);
            }
            catch (Exception e)
            {
                logger.LogCritical(e, "UserController.Get list users catch exception");

                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        // GET api/users/7
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(string id)
        {
            try
            {
                var user = await usersDataService.GetUser(id);

                if (user == null)
                {
                    return NotFound();
                }

                return Ok(user);
            }
            catch (Exception e)
            {
                logger.LogCritical(e, "UserController.Get user catch exception");

                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        // POST api/users
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] User user)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var result = await usersDataService.CreateUser(user);

                    if (result.IsValid)
                    {
                        return Ok(result.Result);
                    }

                    var error = result.GetError();

                    return BadRequest(error);
                }

                return BadRequest(ModelState);
            }
            catch (Exception e)
            {
                logger.LogCritical(e, "UserController.Post catch exception");

                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpGet("init")]
        public async Task<IActionResult> Init()
        {
            try
            {
                await usersDataService.Init();

                return Ok();
            }
            catch (Exception e)
            {
                logger.LogCritical(e, "UserController.Init catch exception");

                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}
