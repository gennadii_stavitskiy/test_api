﻿using Backend.Models;
using Backend.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountsController : ControllerBase
    {
        private readonly AccountDataService accountDataService;
        private readonly ILogger logger;

        public AccountsController(AccountDataService accountDataService, ILoggerFactory factory)
        {
            this.accountDataService = accountDataService;
            this.logger = factory.CreateLogger("Api.UserController");
        }

        // GET api/accounts
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                var accounts = await accountDataService.ListAccount();

                return Ok(accounts);
            }
            catch (Exception e)
            {
                logger.LogCritical(e, "AccountController.Get list users catch exception");

                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        // GET api/accounts/7c21b09f-8c36-4e71-853d-5e6520b42ec1
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(string id)
        {
            try
            {
                var user = await accountDataService.GetAccount(id);

                if (user == null)
                {
                    return NotFound();
                }

                return Ok(user);
            }
            catch (Exception e)
            {
                logger.LogCritical(e, "AccountController.Get account catch exception");

                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        // POST api/accounts
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] Account account)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var result = await accountDataService.CreateAccount(account);

                    if (result.IsValid)
                    {
                        return Ok(result.Result);
                    }

                    var error = result.GetError();

                    return BadRequest(error);
                }

                return BadRequest(ModelState);
            }
            catch (Exception e)
            {
                logger.LogCritical(e, "AccountController.Post catch exception");

                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpGet("init")]
        public async Task<IActionResult> Init()
        {
            try
            {
                await accountDataService.Init();

                return Ok();
            }
            catch (Exception e)
            {
                logger.LogCritical(e, "AccountController.Init catch exception");

                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}
