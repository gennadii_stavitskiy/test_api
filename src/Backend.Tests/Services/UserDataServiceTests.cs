using System.Collections.Generic;
using System.Threading.Tasks;
using Backend.Models;
using Backend.Repositories.DynamoDb;
using Backend.Services;
using Backend.Validation;
using Moq;
using Xunit;

namespace Backend.Tests.Services
{
    public class UserDataServiceTests
    {
        public UserDataServiceTests()
        {
            userDbContext = Mock.Of<IDynamoDbContext<User>>();
            emailToUserDbContext = Mock.Of<IDynamoDbContext<EmailToUser>>();

            service = new UsersDataService(userDbContext, emailToUserDbContext);
        }

        private readonly UsersDataService service;
        private readonly string userId = "7c21b09f-8c36-4e71-853d-5e6520b42ec1";
        private readonly IDynamoDbContext<User> userDbContext;
        private readonly IDynamoDbContext<EmailToUser> emailToUserDbContext;

        [Fact]
        public async Task it_should_get_user()
        {
            Mock.Get(userDbContext).Setup(u => u.GetByIdAsync(userId)).Returns(Task.FromResult(new User {Id = userId}));

            var user = await service.GetUser(userId);

            Assert.NotNull(user);
        }

        [Fact]
        public async Task it_should_list_users()
        {
            Mock.Get(userDbContext).Setup(u => u.GetAllAsync()).Returns(Task.FromResult((IList<User>)new List<User>{ new User { Id = userId } }));

            var users = await service.ListUsers();

            Assert.NotNull(users);
        }

        [Fact]
        public async Task it_should_crete_user()
        {
            var user = new User
            {
                Id = "7c21b09f-8c36-4e71-853d-5e6520b42ec1",
                Name = "test1",
                Email = "test1@example.com",
                MonthlySalary = 5000,
                MonthlyExpenses = 1000
            };
            Mock.Get(userDbContext).Setup(u => u.SaveAsync(user)).Returns(Task.CompletedTask);

            var users = await service.CreateUser(user);

            Mock.Get(userDbContext).Verify(u=> u.SaveAsync(user));
        }

        [Fact]
        public async Task it_should_validate_user()
        {
            var user = new User
            {
                Id = "7c21b09f-8c36-4e71-853d-5e6520b42ec1",
                Name = "test1",
                Email = "test1@example.com",
                MonthlySalary = 5000,
                MonthlyExpenses = 1000
            };
            
            var resultModel = await service.Validate(user, new []{ValidationRules.MonthlyExpensesPositive});

            Assert.True(resultModel.IsValid);
        }

        [Fact]
        public async Task it_should_init()
        {
            await service.Init();

            Mock.Get(userDbContext).Verify(u => u.InitializeSchema());
            Mock.Get(emailToUserDbContext).Verify(e => e.InitializeSchema());
        }
    }
}