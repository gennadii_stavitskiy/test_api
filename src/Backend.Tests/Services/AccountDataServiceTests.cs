using System.Collections.Generic;
using System.Threading.Tasks;
using Backend.Models;
using Backend.Repositories.DynamoDb;
using Backend.Services;
using Moq;
using Xunit;

namespace Backend.Tests.Services
{
    public class AccountDataServiceTests
    {
        public AccountDataServiceTests()
        {
            userDbContext = Mock.Of<IDynamoDbContext<User>>();
            accountContext = Mock.Of<IDynamoDbContext<Account>>();
            emailToUserDbContext = Mock.Of<IDynamoDbContext<EmailToUser>>();

            userDataService = new UsersDataService(userDbContext, emailToUserDbContext);
            service = new AccountDataService(accountContext, userDataService);
        }

        private readonly AccountDataService service;
        private readonly UsersDataService userDataService;
        private readonly string id = "7c21b09f-8c36-4e71-853d-5e6520b42ec1";
        private readonly IDynamoDbContext<User> userDbContext;
        private readonly IDynamoDbContext<Account> accountContext;
        private readonly IDynamoDbContext<EmailToUser> emailToUserDbContext;

        [Fact]
        public async Task it_should_crete_user()
        {
            var user = new User
            {
                Id = "7c21b09f-8c36-4e71-853d-5e6520b42ec1",
                Name = "test1",
                Email = "test1@example.com",
                MonthlySalary = 5000,
                MonthlyExpenses = 1000
            };
            Mock.Get(userDbContext).Setup(u => u.GetByIdAsync("7c21b09f-8c36-4e71-853d-5e6520b42ec1")).Returns(Task.FromResult(user));

            var account = new Account
            {
                Id = "7c21b09f-8c36-4e71-853d-5e6520b42ec1",
                UserId = "7c21b09f-8c36-4e71-853d-5e6520b42ec1"
            };
            Mock.Get(accountContext).Setup(u => u.SaveAsync(account)).Returns(Task.CompletedTask);

            var users = await service.CreateAccount(account);

            Mock.Get(accountContext).Verify(u => u.SaveAsync(account));
        }

        [Fact]
        public async Task it_should_get_account()
        {
            Mock.Get(accountContext).Setup(u => u.GetByIdAsync(id)).Returns(Task.FromResult(new Account {Id = id}));

            var account = await service.GetAccount(id);

            Assert.NotNull(account);
        }

        [Fact]
        public async Task it_should_init()
        {
            await service.Init();

            Mock.Get(accountContext).Verify(e => e.InitializeSchema());
        }

        [Fact]
        public async Task it_should_list_accounts()
        {
            Mock.Get(accountContext).Setup(u => u.GetAllAsync())
                .Returns(Task.FromResult((IList<Account>) new List<Account> {new Account {Id = id}}));

            var account = await service.ListAccount();

            Assert.NotNull(account);
        }
    }
}